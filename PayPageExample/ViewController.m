//
//  ViewController.m
//  PayPageExample
//
//  Created by Shaun Persad on 7/17/15.
//  Copyright (c) 2015 domandtom. All rights reserved.
//

#import "ViewController.h"
#import "AFHTTPRequestOperationManager.h"
#include <stdlib.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    int consumer_id = 3;
    
    NSString *lisa_api_url = [NSString stringWithFormat:@"http://lisa.dev:1337/consumers/%d/credit-cards/create?access_token=u9Ub05JiI6OO5mX8AMRxyaGrS3UbOoZKnksucG0VFobZJbd7Hp2J5uu7Bl1pdCfiY36rVYMPh62uUptZEhEeXPxPcdnymVHv0Mmf", consumer_id];
    
    NSString *vantiv_api_url = @"https://request-prelive.np-securepaypage-litle.com/LitlePayPage/paypage";
    
    NSString *paypage_id = @"bjc29rHNm3bEsK75"; //pre-live id. this will change for production.
    NSString *report_group = @"Default Report Group";
    NSString *order_id = [NSString stringWithFormat:@"%d_%d", consumer_id, arc4random_uniform(74)]; //user's id + underscore + random
    NSString *credit_card_number = @"4457119922390123";
    NSString *cvv = @"123";
    
    NSDictionary *parameters = @{
                                 @"paypageId": paypage_id,
                                 @"reportGroup": report_group,
                                 @"orderId": order_id,
                                 @"accountNumber": credit_card_number,
                                 @"cvv": cvv,
                                 @"id": order_id
                                 };

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/x-javascript"];

    /*
     * Call to Vantiv's API
     */
    [manager POST:vantiv_api_url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Vantiv response: %@", responseObject);
        
        NSDictionary *parameters = @{
                       @"bin": [responseObject objectForKey:@"bin"],
                       @"last_four": [responseObject objectForKey:@"lastFour"],
                       @"type": [responseObject objectForKey:@"type"],
                       @"paypage_registration_id": [responseObject objectForKey:@"paypageRegistrationId"],
                       @"order_id": [responseObject objectForKey:@"orderId"],
                       @"expiration_date": @"1112"
                       };
        
        /*
         * Call to Lisa's API
         */
        [[AFHTTPRequestOperationManager manager] POST:lisa_api_url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"credit card object: %@", responseObject);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
         
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
